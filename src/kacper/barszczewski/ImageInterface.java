package kacper.barszczewski;

import java.awt.image.BufferedImage;

public interface ImageInterface {

    BufferedImage getImage();

    void setImage(BufferedImage image);

    int getImageWidth();

    int getImageHeight();

    void setImageNoNotify(BufferedImage image);

    void addListener(ImageListener listener);

}
