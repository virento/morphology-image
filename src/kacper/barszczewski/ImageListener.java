package kacper.barszczewski;

public interface ImageListener {

    void onImageChange();
}
