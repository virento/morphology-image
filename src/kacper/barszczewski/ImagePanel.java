package kacper.barszczewski;

import kacper.barszczewski.components.LoadComponent;
import kacper.barszczewski.components.MorphologyComponent;
import kacper.barszczewski.components.PropagationComponent;

import javax.swing.*;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.List;

public class ImagePanel extends JPanel implements ImageInterface {

    private BufferedImage image = null;

    private List<ImageListener> listenerList = new ArrayList<>();

    public ImagePanel() {
        add(new ImagePainter());
        JPanel componentPanel = new JPanel();
        componentPanel.setLayout(new BoxLayout(componentPanel, BoxLayout.PAGE_AXIS));
        componentPanel.setBackground(Color.RED);
        componentPanel.add(new LoadComponent(this));
        componentPanel.add(new PropagationComponent(this));
        componentPanel.add(new MorphologyComponent(this));
        add(componentPanel);
    }

    @Override
    public BufferedImage getImage() {
        return image;
    }

    @Override
    public void setImage(BufferedImage image) {
        this.image = image;
        for (ImageListener imageListener : listenerList) {
            imageListener.onImageChange();
        }
        repaint();
    }

    @Override
    public int getImageWidth() {
        return 800;
    }

    @Override
    public int getImageHeight() {
        return 600;
    }

    @Override
    public void setImageNoNotify(BufferedImage image) {
        this.image = image;
        repaint();
    }

    @Override
    public void addListener(ImageListener listener) {
        listenerList.add(listener);
    }

    public final class ImagePainter extends JPanel {

        public ImagePainter() {
            setPreferredSize(new Dimension(800, 600));
        }

        @Override
        protected void paintComponent(Graphics g) {
            if (image == null) {
                g.setColor(Color.LIGHT_GRAY);
                g.fillRect(0, 0, getWidth(), getHeight());
            } else {
                g.drawImage(image, 0, 0, getImageWidth(), getImageHeight(), null);
            }
        }
    }

}
