package kacper.barszczewski;

import javax.swing.*;
import java.awt.*;

public class MainApplication extends JFrame {

    public MainApplication() {
        setTitle("Zadanie 8");
        add(new ImagePanel());

        setDefaultCloseOperation(EXIT_ON_CLOSE);
        setVisible(true);
        pack();
    }


    public static void main(String[] args) {
        EventQueue.invokeLater(MainApplication::new);
    }
}
