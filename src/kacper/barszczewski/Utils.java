package kacper.barszczewski;

import java.awt.image.BufferedImage;
import java.util.Arrays;

public class Utils {

    public static int[][][] rotate(int[][][] in) {
        int[][][] out = new int[in.length][3][3];
        for (int i = 0; i < in.length; i++) {
            out[i][0][0] = in[i][2][0];
            out[i][1][0] = in[i][2][1];
            out[i][2][0] = in[i][2][2];

            out[i][0][1] = in[i][1][0];
            out[i][1][1] = in[i][1][1];
            out[i][2][1] = in[i][1][2];

            out[i][0][2] = in[i][0][0];
            out[i][1][2] = in[i][0][1];
            out[i][2][2] = in[i][0][2];
        }
        return out;
    }

    public static int[][] rotate(int[][] in) {
        int[][] out = new int[3][3];
        for (int i = 0; i < in.length; i++) {
            out[0][0] = in[2][0];
            out[1][0] = in[2][1];
            out[2][0] = in[2][2];

            out[0][1] = in[1][0];
            out[1][1] = in[1][1];
            out[2][1] = in[1][2];

            out[0][2] = in[0][0];
            out[1][2] = in[0][1];
            out[2][2] = in[0][2];
        }
        return out;
    }

    public interface Operation {

        int[] operation(int[] values);
    }

    public interface FilterOperation {
        int[] operation(int[][][] values);
    }

    public static BufferedImage sobel(BufferedImage image) {
        int[][] sobel1 = new int[][]{
                new int[]{1, 2, 1},
                new int[]{0, 0, 0},
                new int[]{-1, -2, -1},
        };
        int[][] sobel2 = new int[][]{
                new int[]{1, 0, -1},
                new int[]{2, 0, -2},
                new int[]{1, 0, -1},
        };
        return filterOperation(image, sobel1, values -> {
            int[] sum = new int[3];
            for (int i = 0; i < sobel1.length; i++) {
                for (int j = 0; j < sobel1.length; j++) {
                    sum[0] += values[i][j][0] * sobel1[i][j] + values[i][j][0] * sobel2[i][j];
                    sum[1] += values[i][j][1] * sobel1[i][j] + values[i][j][1] * sobel2[i][j];
                    sum[2] += values[i][j][2] * sobel1[i][j] + values[i][j][2] * sobel2[i][j];
                }
            }
            fixRGB(sum);
            return sum;
        });
    }

    private static void fixRGB(int[] sum) {
        if (sum[0] < 0) sum[0] = 0;
        if (sum[1] < 0) sum[1] = 0;
        if (sum[2] < 0) sum[2] = 0;
        if (sum[0] > 255) sum[0] = 255;
        if (sum[1] > 255) sum[1] = 255;
        if (sum[2] > 255) sum[2] = 255;
    }

    public static BufferedImage median(BufferedImage image) {
        int[][] mask = new int[][]{
                new int[]{1, 1, 1},
                new int[]{1, 1, 1},
                new int[]{1, 1, 1}
        };

        return filterOperation(image, mask, values -> {
            int[] numbers = new int[values.length * values[0].length];
            int[] finalValues = new int[3];
            for (int i = 0; i < 3; i++) {
                int index = 0;
                for (int j = 0; j < values.length; j++) {
                    for (int k = 0; k < values[0].length; k++) {
                        numbers[index++] = values[j][j][i];
                    }
                }
                Arrays.sort(numbers);
                finalValues[i] = numbers[4];
            }
            return finalValues;
        });
    }

    public static BufferedImage filter(BufferedImage image, int[][] mask) {
        int tempMaskSum = 0;
        for (int[] ints : mask) {
            for (int anInt : ints) {
                tempMaskSum += anInt;
            }
        }
        final int maskSum = tempMaskSum;

        return filterOperation(image, mask, values -> {
            int[] sum = new int[3];
            for (int i = 0; i < mask.length; i++) {
                for (int j = 0; j < mask.length; j++) {
                    sum[0] += values[i][j][0] * mask[i][j];
                    sum[1] += values[i][j][1] * mask[i][j];
                    sum[2] += values[i][j][2] * mask[i][j];
                }
            }
            if (maskSum != 0) {
                sum[0] /= maskSum;
                sum[1] /= maskSum;
                sum[2] /= maskSum;
            }
            fixRGB(sum);
            return sum;
        });
    }

    public static BufferedImage filterOperation(BufferedImage image, int[][] mask, FilterOperation operation) {
        BufferedImage newImage = new BufferedImage(image.getWidth(), image.getHeight(), BufferedImage.TYPE_INT_RGB);
        int[] rgbTable = image.getRGB(0, 0, image.getWidth(), image.getHeight(), null, 0, image.getWidth());

        int mXFrom = -(mask[0].length - 1) / 2;
        int mXTo = -1 * mXFrom;
        int mYFrom = -(mask[1].length - 1) / 2;
        int mYTo = -1 * mYFrom;
        int[] newRgb = new int[rgbTable.length];
        for (int i = 0; i < rgbTable.length; i++) {
            int[][][] value = new int[mask.length][mask.length][3];
            for (int k = mXFrom; k < mXTo + 1; k++) { //x
                for (int l = mYFrom; l < mYTo + 1; l++) { //y
                    int y = i / image.getWidth();
                    int x = i - y * image.getWidth();
                    if (x + k < 0) {
                        x = 0;
                    } else if (x + k >= image.getWidth()) {
                        x = image.getWidth() - 1;
                    } else {
                        x = x + k;
                    }
                    if (y + l < 0) {
                        y = 0;
                    } else if (y + l >= image.getHeight()) {
                        y = image.getHeight() - 1;
                    } else {
                        y = y + l;
                    }

                    int index = x + y * image.getWidth();
                    int rgb = rgbTable[index];
                    int[] rgbValue = new int[]{
                            (rgb >> 16) & 0xFF,
                            (rgb >> 8) & 0xFF,
                            rgb & 0xFF
                    };
                    value[k - mXFrom][l - mYFrom][0] = rgbValue[0];
                    value[k - mXFrom][l - mYFrom][1] = rgbValue[1];
                    value[k - mXFrom][l - mYFrom][2] = rgbValue[2];
                }
            }
            int[] finalRgb = operation.operation(value);
            newRgb[i] = getRGB(finalRgb[0], finalRgb[1], finalRgb[2]);
        }
        newImage.setRGB(0, 0, image.getWidth(), image.getHeight(), newRgb, 0, image.getWidth());
        return newImage;
    }

    public static BufferedImage gray(BufferedImage image, int value) {
        return doOperation(image, values -> {
            values[0] = values[value];
            values[1] = values[value];
            values[2] = values[value];
            return values;
        });
    }

    public static BufferedImage power(BufferedImage image, final float value) {
        return doOperation(image, values -> {
            values[0] = (int) (value * Math.pow(values[0], 2));
            values[1] = (int) (value * Math.pow(values[1], 2));
            values[2] = (int) (value * Math.pow(values[2], 2));
            return values;
        });
    }

    public static BufferedImage log(BufferedImage image, final float value) {
        return doOperation(image, values -> {
            values[0] = (int) (value * Math.log(values[0] + 1));
            values[1] = (int) (value * Math.log(values[1] + 1));
            values[2] = (int) (value * Math.log(values[2] + 1));
            return values;
        });
    }

    public static BufferedImage multiply(BufferedImage image, final int value) {
        return doOperation(image, values -> {
            values[0] = values[0] * value;
            values[1] = values[1] * value;
            values[2] = values[2] * value;
            return values;
        });
    }

    public static BufferedImage divide(BufferedImage image, final int value) {
        return doOperation(image, values -> {
            if (value > 0) {
                values[0] = values[0] / value;
                values[1] = values[1] / value;
                values[2] = values[2] / value;
            }
            return values;
        });
    }

    public static BufferedImage add(BufferedImage image, int value) {
        return doOperation(image, getAddOperation(value));
    }

    public static BufferedImage doOperation(BufferedImage image, Operation operation) {
        int[] rgbTable = image.getRGB(0, 0, image.getWidth(), image.getHeight(), null, 0, image.getWidth());
        for (int i = 0; i < rgbTable.length; i++) {
            int rgb = rgbTable[i];
            int[] rgbValue = new int[]{
                    (rgb >> 16) & 0xFF,
                    (rgb >> 8) & 0xFF,
                    rgb & 0xFF
            };
            rgbValue = operation.operation(rgbValue);
            if (rgbValue[0] < 0) rgbValue[0] = 0;
            else if (rgbValue[0] > 255) rgbValue[0] = 255;
            if (rgbValue[1] < 0) rgbValue[1] = 0;
            else if (rgbValue[1] > 255) rgbValue[1] = 255;
            if (rgbValue[2] < 0) rgbValue[2] = 0;
            else if (rgbValue[2] > 255) rgbValue[2] = 255;
            rgbTable[i] = getRGB(rgbValue[0], rgbValue[1], rgbValue[2]);
        }
        BufferedImage newImage = new BufferedImage(image.getWidth(), image.getHeight(), image.getType());
        newImage.setRGB(0, 0, image.getWidth(), image.getHeight(), rgbTable, 0, image.getWidth());
        return newImage;
    }

    public static int getRGB(int red, int green, int blue) {
        int rgbInt = red;
        rgbInt = (rgbInt << 8) + green;
        rgbInt = (rgbInt << 8) + blue;
        return rgbInt;
    }

    public static Operation getAddOperation(int value) {
        return values -> {
            values[0] += value % 256;
            values[1] += value % 256;
            values[2] += value % 256;
            return values;
        };
    }

    public static BufferedImage toImage(int[][] rgb, BufferedImage image) {
        BufferedImage newImage = new BufferedImage(image.getWidth(), image.getHeight(), image.getType());
        int[] table = new int[rgb[0].length];
        for (int i = 0; i < table.length; i++) {
            table[i] = getRGB(rgb[0][i], rgb[1][i], rgb[2][i]);
        }
        newImage.setRGB(0, 0, image.getWidth(), image.getHeight(), table, 0, image.getWidth());
        return newImage;
    }

    public static int[] getColor(BufferedImage image, int index) {
        int[] rgbTable = image.getRGB(0, 0, image.getWidth(), image.getHeight(), null, 0, image.getWidth());
        int[] values = new int[rgbTable.length];
        for (int i = 0; i < rgbTable.length; i++) {
            int rgb = rgbTable[i];
            int[] rgbValue = new int[]{
                    (rgb >> 16) & 0xFF,
                    (rgb >> 8) & 0xFF,
                    rgb & 0xFF
            };
            values[i] = rgbValue[index];
        }
        return values;
    }

    public static int[] toHistogram(int[] values) {
        int[] histogram = new int[256];
        for (int value : values) {
            histogram[value]++;
        }
        return histogram;
    }

    public static int parseToInt(String s) {
        try {
            return Integer.parseInt(s);
        } catch (NumberFormatException e) {
            throw new RuntimeException("Can't parse " + s + " to valid number");
        }
    }

    public static int[] percentPropagation(BufferedImage image, int percent) {
        int[] table = toHistogram(getColor(image, 0));
        int sum = Arrays.stream(table).sum();
        double limes = ((double) percent / 100) * sum;
        int nextSum = 0;

        int[] LUT = new int[256];
        for (int i = 0; i < LUT.length; i++) {
            nextSum += table[i];
            LUT[i] = nextSum >= limes ? 255 : 0;
        }

        return LUT;
    }

    public static int otsuAlgorithm(BufferedImage image) {
        int[] histogram = toHistogram(getColor(image, 0));
        int sum = Arrays.stream(histogram).sum();
        int threshold = 0;
        float maxVarW = Float.MAX_VALUE;
        for (int i = 0; i < histogram.length; i++) {
            int sumB = Arrays.stream(histogram, 0, i).sum();
            int sumF = Arrays.stream(histogram, i, histogram.length).sum();
            float wb = sumB / (float) sum;
            float wf = sumF / (float) sum;

            int sumMb = 0;
            int sumMf = 0;
            for (int j = 0; j < i; j++) {
                sumMb += j * histogram[j];
            }
            for (int j = i; j < histogram.length; j++) {
                sumMf += j * histogram[j];
            }
            float meanB = sumMb / (float) sumB;
            float meanF = sumMf / (float) sumF;

            float varB = 0;
            float varF = 0;
            for (int j = 0; j < i; j++) {
                varB += histogram[j] * Math.pow(j - meanB, 2);
            }
            for (int j = i; j < histogram.length; j++) {
                varF += histogram[j] * Math.pow(j - meanF, 2);
            }
            varB /= sumB;
            varF /= sumF;

            float varW = wf * varF * varF + wb * varB * varB;
            if (maxVarW > varW && varW != 0) {
                maxVarW = varW;
                threshold = i;
            }
        }
        System.out.println(threshold);
        return threshold;
    }
}
