package kacper.barszczewski.components;

import kacper.barszczewski.ImageInterface;

import javax.swing.*;
import java.awt.*;
import java.awt.image.BufferedImage;

public abstract class CustomComponent extends JPanel {

    private final ImageInterface imageInterface;

    public CustomComponent(ImageInterface imageInterface) {
        this.imageInterface = imageInterface;
    }

    public BufferedImage getImage() {
        return imageInterface.getImage();
    }

    public void setImage(BufferedImage image) {
        imageInterface.setImage(image);
    }

    public void setImageNoNotify(BufferedImage image) {
        imageInterface.setImageNoNotify(image);
    }

    protected int validateNumber(String textValue) {
        try {
            return Integer.parseInt(textValue);
        } catch (NumberFormatException e) {
            JOptionPane.showMessageDialog(this, textValue + " is not a number");
            throw new RuntimeException(e);
        }
    }

    protected float validateFloatNumber(String textValue) {
        try {
            return Float.parseFloat(textValue);
        } catch (NumberFormatException e) {
            JOptionPane.showMessageDialog(this, textValue + " is not a float number");
            throw new RuntimeException(e);
        }
    }

    protected Component createInput() {
        JTextField field = new JTextField();
        field.setPreferredSize(new Dimension(50, 24));
        return field;
    }

    protected Component createButton(String text) {
        JButton button = new JButton(text);
        return button;
    }

    public int getImageWidth() {
        return imageInterface.getImageWidth();
    }

    public int getImageHeight() {
        return imageInterface.getImageHeight();
    }
}
