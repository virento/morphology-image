package kacper.barszczewski.components;

import kacper.barszczewski.ImagePanel;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

public class LoadComponent extends CustomComponent {

    private BufferedImage copy;

    public LoadComponent(ImagePanel imagePanel) {
        super(imagePanel);
        setBorder(BorderFactory.createTitledBorder("Load image"));
        JButton button = (JButton) createButton("Load image");
        button.addActionListener(e -> {
            JFileChooser fileChooser = new JFileChooser();
            int returnCode = fileChooser.showOpenDialog(this);
            if (returnCode == JFileChooser.APPROVE_OPTION) {
                File file = fileChooser.getSelectedFile();
                BufferedImage image = null;
                try {
                    image = ImageIO.read(file);
                } catch (IOException ioException) {
                    ioException.printStackTrace();
                }
                image = rescale(image);
                copy = rescale(image);
                setImage(image);
                System.out.println("Selected file: " + file);
            }
        });

        JButton refreshButton = (JButton) createButton("Refresh");
        refreshButton.addActionListener(e -> setImage(rescale(copy)));
        add(button);
        add(refreshButton);
    }

    private BufferedImage rescale(BufferedImage image) {
        Image tmp = image.getScaledInstance(image.getWidth(), image.getHeight(), Image.SCALE_SMOOTH);
        BufferedImage newImage = new BufferedImage(image.getWidth(), image.getHeight(), BufferedImage.TYPE_INT_RGB);
        Graphics2D g2d = newImage.createGraphics();
        g2d.drawImage(tmp, 0, 0, null);
        g2d.dispose();
        return newImage;
    }
}
