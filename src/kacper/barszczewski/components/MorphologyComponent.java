package kacper.barszczewski.components;

import kacper.barszczewski.ImageInterface;
import kacper.barszczewski.Utils;

import javax.swing.*;
import java.awt.image.BufferedImage;
import java.util.concurrent.atomic.AtomicBoolean;

public class MorphologyComponent extends CustomComponent {

    final int[][] mask = new int[][]{
            {1, 1, 1},
            {1, 1, 1},
            {1, 1, 1}
    };

    final int[][] thin2 = new int[][]{
            {255, 255, 255},
            {-1, 0, -1},
            {0, 0, 0}
    };
    final int[][] thin1 = new int[][]{
            {-1, 255, 255},
            {0, 0, 255},
            {-1, 0, -1}
    };

    final int[][] thic1 = new int[][] {
            {0, 0, -1},
            {0, 255, -1},
            {0, -1, 255}
    };
    final int[][] thic2 = new int[][] {
            {-1, 0, 0},
            {-1, 255, 0},
            {0, -1, 255}
    };

    final int[][] thin290, thin2180, thin2270, thin190, thin1180, thin1270;
    final int[][] thic190,thic1180, thic1270, thic290, thic2180, thic2270;

    public MorphologyComponent(ImageInterface imageInterface) {
        super(imageInterface);
        thin290 = Utils.rotate(thin2);
        thin2180 = Utils.rotate(thin290);
        thin2270 = Utils.rotate(thin2180);
        thin190 = Utils.rotate(thin1);
        thin1180 = Utils.rotate(thin190);
        thin1270 = Utils.rotate(thin1180);

        thic190 = Utils.rotate(thic1);
        thic1180 = Utils.rotate(thic190);
        thic1270 = Utils.rotate(thic1180);
        thic290 = Utils.rotate(thic2);
        thic2180 = Utils.rotate(thic290);
        thic2270 = Utils.rotate(thic2180);
        setBorder(BorderFactory.createTitledBorder("Morphology operations"));
        add(createDilationButton());
        add(createErosionButton());
        add(createThinButton());
        add(createThicButton());
    }

    private JButton createThicButton() {
        JButton button = (JButton) createButton("Thic");
        button.addActionListener(e -> new Thread(() -> {
            AtomicBoolean wasTouched = new AtomicBoolean(true);
            BufferedImage image = getImage();
            int iter = 0;
            while (wasTouched.get()) {
                iter++;
                wasTouched.set(false);
                image = ham(image, thic1, wasTouched,0);
                image = ham(image, thic2, wasTouched,0);
                image = ham(image, thic190, wasTouched,0);
                image = ham(image, thic1180, wasTouched,0);
                image = ham(image, thic1270, wasTouched,0);
                image = ham(image, thic290, wasTouched,0);
                image = ham(image, thic2180, wasTouched,0);
                image = ham(image, thic2270, wasTouched, 0);
                setImageNoNotify(image);
            }
            System.out.println("Done in " + iter + " iterations");
        }).start());
        return button;
    }

    private JButton createThinButton() {
        JButton button = (JButton) createButton("Thin");
        button.addActionListener(e -> new Thread(() -> {
            AtomicBoolean wasTouched = new AtomicBoolean(true);
            BufferedImage image = getImage();
            int iter = 0;
            while (wasTouched.get()) {
                iter++;
                wasTouched.set(false);
                image = ham(image, thin1, wasTouched, 255);
                image = ham(image, thin190, wasTouched, 255);
                image = ham(image, thin1180, wasTouched, 255);
                image = ham(image, thin1270, wasTouched, 255);
                image = ham(image, thin2, wasTouched, 255);
                image = ham(image, thin290, wasTouched, 255);
                image = ham(image, thin2180, wasTouched, 255);
                image = ham(image, thin2270, wasTouched,255);
                setImageNoNotify(image);
            }
            System.out.println("Done in " + iter + " iterations");
        }).start());
        return button;
    }

    private BufferedImage ham(BufferedImage image, int[][] thin, AtomicBoolean wasTouched, final int value) {
        return Utils.filterOperation(image, mask, values -> {
            if (isMaskEqual2(values, thin)) {
                wasTouched.set(true);
                return new int[]{value, value, value};
            }
            return new int[]{values[1][1][0], values[1][1][1], values[1][1][2]};
        });
    }

    private boolean isMaskEqual2(int[][][] values, int[][] thin2) {
        for (int i = 0; i < values.length; i++) {
            for (int j = 0; j < values[0].length; j++) {
                if (thin2[i][j] != -1 && thin2[i][j] != values[i][j][0]) {
                    return false;
                }
            }
        }
        return true;
    }

    private boolean isMaskEqual(int[][][] values, int[][][] mask) {
        boolean isEqual;
        for (int m = 0; m < mask.length; m++) {
            isEqual = true;
            for (int i = 0; i < values.length; i++) {
                for (int j = 0; j < values[0].length; j++) {
                    if (values[i][j][0] != mask[m][i][j]) {
                        isEqual = false;
                    }
                }
            }
            if (isEqual) {
                return true;
            }
        }
        return false;
    }

    private JButton createDilationButton() {
        JButton button = (JButton) createButton("Dilation");
        button.addActionListener(e -> {
            BufferedImage image = Utils.filterOperation(getImage(), mask, values -> {
                for (int i = 0; i < mask.length; i++) {
                    for (int j = 0; j < mask.length; j++) {
                        if (values[i][j][0] == 0) {
                            return new int[]{0, 0, 0};
                        }
                    }
                }
                return new int[]{255, 255, 255};
            });
            setImageNoNotify(image);
        });
        return button;
    }

    private JButton createErosionButton() {
        JButton button = (JButton) createButton("Erosion");
        button.addActionListener(e -> {
            BufferedImage image = Utils.filterOperation(getImage(), mask, values -> {
                for (int i = 0; i < mask.length; i++) {
                    for (int j = 0; j < mask.length; j++) {
                        if (values[i][j][0] == 255) {
                            return new int[]{255, 255, 255};
                        }
                    }
                }
                return new int[]{0, 0, 0};
            });
            setImageNoNotify(image);
        });
        return button;
    }
}
