package kacper.barszczewski.components;

import kacper.barszczewski.ImageInterface;
import kacper.barszczewski.ImageListener;
import kacper.barszczewski.Utils;

import javax.swing.*;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.List;

public class PropagationComponent extends CustomComponent implements ImageListener {

    private final List<JRadioButton> jRadioButtonList = new ArrayList<>();

    private final BufferedImage[] imageRGB = new BufferedImage[3];

    private JSlider slider;

    public PropagationComponent(ImageInterface imageInterface) {
        super(imageInterface);
        setLayout(new BoxLayout(this, BoxLayout.PAGE_AXIS));
        add(createJRationButtonPanel());
        add(createManualSlider());
        add(createPercentPropagation());
        add(createOsuPropagation());
        imageInterface.addListener(this);
    }

    private Component createOsuPropagation() {
        JPanel panel = new JPanel();
        panel.setBorder(BorderFactory.createTitledBorder("Otsu Algorithm"));
        JButton button = (JButton) createButton("Otsu Algorithm");
        button.addActionListener(e -> {
            int threshold = Utils.otsuAlgorithm(imageRGB[getSelectedColorIndex()]);
            BufferedImage image = Utils.doOperation(imageRGB[getSelectedColorIndex()], values -> {
                if (values[0] >= threshold) {
                    return new int[]{255, 255, 255};
                }
                return new int[]{0, 0, 0};
            });
            setImageNoNotify(image);
        });
        panel.add(button);
        return panel;
    }

    private Component createPercentPropagation() {
        JPanel panel = new JPanel();
        panel.setBorder(BorderFactory.createTitledBorder("Percent propagation"));
        JTextField text = (JTextField) createInput();
        JButton button = (JButton) createButton("Apply");
        panel.add(text);
        panel.add(button);
        button.addActionListener(e -> {
            int[] lut = Utils.percentPropagation(imageRGB[getSelectedColorIndex()], Utils.parseToInt(text.getText()));
            BufferedImage image = Utils.doOperation(imageRGB[getSelectedColorIndex()], values -> new int[]{lut[values[0]], lut[values[0]], lut[values[0]]});
            setImageNoNotify(image);
        });
        return panel;
    }

    private Component createJRationButtonPanel() {
        JPanel panel = new JPanel();
        panel.add(createJRation("Red"));
        panel.add(createJRation("Green"));
        panel.add(createJRation("Blue"));
        return panel;
    }

    private Component createJRation(String text) {
        JRadioButton button = new JRadioButton(text);
        button.addActionListener(e -> {
            computeImage(slider);
            clearButtons();
            button.setSelected(true);
        });
        jRadioButtonList.add(button);
        return button;
    }

    private void clearButtons() {
        jRadioButtonList.forEach(jRadioButton -> {
            jRadioButton.setSelected(false);
        });
    }

    private Component createManualSlider() {
        JPanel sliderPanel = new JPanel();
        sliderPanel.setBorder(BorderFactory.createTitledBorder("Manual propagation"));
        JSlider slider = new JSlider(0, 255);
        JTextField field = (JTextField) createInput();
        field.setText(String.valueOf(slider.getValue()));
        slider.addChangeListener(e -> {
            int value = slider.getValue();
            field.setText(String.valueOf(value));
            computeImage(slider);
        });
        field.addActionListener(e -> {
            int i = Utils.parseToInt(field.getText());
            slider.setValue(i);
        });

        sliderPanel.add(slider);
        sliderPanel.add(field);
        this.slider = slider;
        return sliderPanel;
    }

    private void computeImage(JSlider slider) {
        BufferedImage image = Utils.doOperation(imageRGB[getSelectedColorIndex()], values -> {
            int newValue;
            if (values[0] > slider.getValue()) {
                newValue = 255;
            } else {
                newValue = 0;
            }
            return new int[]{newValue, newValue, newValue};
        });
        setImageNoNotify(image);
    }

    private int getSelectedColorIndex() {
        for (int i = 0; i < jRadioButtonList.size(); i++) {
            JRadioButton button = jRadioButtonList.get(i);
            if (button.isSelected()) {
                return i;
            }
        }
        return 0;
    }

    @Override
    public void onImageChange() {
        int[] r = Utils.getColor(getImage(), 0);
        int[] g = Utils.getColor(getImage(), 1);
        int[] b = Utils.getColor(getImage(), 2);
        imageRGB[0] = Utils.toImage(new int[][]{r, r, r}, getImage());
        imageRGB[1] = Utils.toImage(new int[][]{g, g, g}, getImage());
        imageRGB[2] = Utils.toImage(new int[][]{b, b, b}, getImage());
    }
}
